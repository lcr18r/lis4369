# LIS4369 Extensible Enterprise Soluctions

## Lucia Ramirez

### Assignment 2 Requirements:

*three parts:*

1. Payroll calculator
2. Bitbucket repo links:

      a) this assignment and

      b) the completed tutorial (bitbucketstationlocations)

3. Skill Sets 1, 2, and 3

#### README.md file includes the following items:

* Screenshot of a2_payroll_calculator application running
* Link to A2 .ipynb file: [payroll_calculator.ipynb](https://bitbucket.org/lcr18r/lis4369/src/master/a2/payroll_calculator.ipynb "Calculator/a2_payroll_calculator.ipynb")
* Provide screenshots for completed python skill sets:

#### Assignment Screenshots:

| Payroll No Overtime | Payroll With Overtime |
| :-------- | :------ |
|![A2 Payroll Calculator without Overtime](img/a2_no_ot.PNG "A2 Payroll without Overtime") | ![A2 Payroll Calculator with Overtime](img/a2_with_ot.PNG "A2 Payroll with Overtime")


| Skill Set 1: Square Feet to Acres | Skill Set 2: Miles Per Gallon | Skill Set 3: IT/ICT Student Percentage |
| :--------- | : --------- | : --------- |
| ![Skill Set 1 Screenshot](img/SS1.PNG "Skill Set 1") | ![Skill Set 2 Screenshot](img/SS2.PNG "Skill Set 2") | ![Skill Set 3 Screenshot](img/SS3.PNG "Skill Set 3") |

### Jupyter Notebook Screenshots:
![Payroll Calculator in Jupyter Notebook](img/a2_jupyter_nb.PNG "Payroll Calculator in Jupyter Notebook")
![Payroll Calculator in Jupyter Notebook](img/a2_jupyter_nbb.PNG "Payroll Calculator in Jupyter Notebook")


| Payroll No Overtime | Payroll With Overtime |
| :-------- | :_----- |
|![Payroll No Overtime in Jupyter Notebook](img/a2_jupyter_nb2.PNG "Payroll No Overtime") | ![Payroll with Overtime Jupyter Notebook](img/a2_jupyter_nb3.PNG "Payroll with Overtime")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/lcr18r/bitbucketstationlocations/ "Bitbucket Station Locations")