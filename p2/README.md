# LIS4369 Extensible Enterprise Solutions 

## Lucia Ramirez

### Assignment: Project 2 Requirements:

*two parts:*

#### This README.md file includes the following items:
1. RStudio: Motor Trend Car Road Tests data analysis
    - Screenshot of [Lucia_Ramirez_p2_requirements](https://bitbucket.org/lcr18r/lis4369/src/6f4ec58b7f49885eab1f7f8d8c82431309b10b2a/p2/Lucia_Ramirez_p2_requirements.txt "Lucia_Ramirez_p2_requirements") text file output.
    - Screenshot of graphs from p2 output, including developer name.

2. RStudio: Annual Developer Survey data analysis
    - Screenshot of *Annual Developer Survey* running.
    - Screenshot of graphs from [myplotfile](annual_developer_survey/myplotfile.pdf "soads graph images")


#### Assignment Screenshots:

##### 1.  Motor Trend Car Road Tests Screenshots: 
| Text File Images | 
| :-------- | :-------- | 
| ![P2 text image](img/p2_1.png "Project 2 text file image") | ![P2 text image](img/p2_2.png "Project 2 text file image") | 

| Scatter Plots | |
| :-------- | :-------- | 
| ![P2 Scatterplot image of Displacement vs MPG](plot_disp_and_mpg_1.png "P2 Scatterplot image of Displacement vs MPG") | ![P2 Scatterplot image of Weight vs MPG](plot_disp_and_mpg_2.png "P2 Scatterplot image of Weight vs MPG") |

##### 2.  Annual Developer Survey Screenshots:
| RStudio Running |
| :-------- |  
| ![soads program running](img/soads_0.png "SOADS program") | 

| Graphs |
| :-------- | :-------- |  :-------- | 
| ![soads topo.colors image](img/soads_1.png "SOADS topo.colors") | ![Distribution of SOADS 2019 Ages columns](img/soads_2.png "Distribution of SOADS 2019 Ages columns") | ![Distribution of SOADS 2019 Ages](img/soads_3.png "Distribution of SOADS 2019 Ages") |



