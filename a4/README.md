# LIS4369 Extensible Enterprise Soluctions

## Lucia Ramirez

### Assignment 4 Requirements:

#### This README.md file includes the following items:
1. Data Analysis 2 in Terminal
    - Screenshot of *Data Analysis 2* application running.

2. Link to a4_data_analysis_2.ipynb file: [data_analysis_2.ipynb](https://bitbucket.org/lcr18r/lis4369/src/master/a4/data_analysis_2.ipynb "data_analysis_2.ipynb").

3. Data Analysis 2 in Jupyter Notebook

4. Skill Sets:
    - Skill Set 10
        - Python Using Dictionaries
    - Skill Set 11
        - Pseudo Random Number Generator
    - Skill Set 12
        - Temperature Converter

#### Assignment Screenshots:
##### 1.  Data Analysis 2 in Terminal
| **Screenshot** |
| :-------- | 
| ![a4 Data Analysis](img/a4.png "a4 Data Analysis") |
| **Plot Screenshot** |
| ![a4 Data Analysis Plots](img/a4_graph.png "a4 Data Analysis Plots") |

##### 3.  Data Analysis 2 in Jupyter Notebook Screenshots:
| **Code** | 
| :-------- | 
|![Data Analysis Code in Jupyter Notebook](img/a4_jn_1.png "Data Analysis Code") | 

| **Output** |
| :-------- | 
|![Data Analysis Output Jupyter Notebook](img/a4_jn_2.png "Data Analysis") |
|![Data Analysis Output Jupyter Notebook](img/a4_jn_2.png "Data Analysis") |
|![Data Analysis Output Jupyter Notebook](img/a4_jn_3.png "Data Analysis") |
|![Data Analysis Output Jupyter Notebook](img/a4_jn_4.png "Data Analysis") |


##### 4. Skill Sets Screenshots
| Skill Set 10: Dictionaries | Skill Set 11: Random Number Generator | Skill Set 12: Temperature Conversion | 
| :--------- | : --------- | : --------- |
| ![Skill Set 10 Screenshot](img/SS10.png "Skill Set 10") | ![Skill Set 11 Screenshot](img/ss11.png "Skill Set 11") |![Skill Set 12 Screenshot](img/ss12.png "Skill Set 12") |





