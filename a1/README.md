# LIS4369 Extensible Enterprise Soluctions (Python/R Data Analytics/Visualization)

## Lucia Ramirez

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and BitBucket
2. Development Installations
3. Questions
4. Bitbucket repo links:

      a) this assignment and

      b) the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1 .ipynb file: [tip_calculator.ipynb](https://bitbucket.org/lcr18r/lis4369/src/master/a1/a1_tip_calculator/tip_calculator.ipynb)
* git commands w/short descriptions


#### Git commands w/short descriptions:

1. git init -  Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree statusgit
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git fetch - Download objects and refs from another repository
8. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

#### Screenshot of a1_tip_calculator application running (IDLE):

![Python Installation Screenshot IDLE](img/a1_tip_calculator_idle.PNG)

#### Screenshot of a1_tip_calculator application running (Visual Studio Code):

![Python Installation Screenshot VS Code](img/a1_tip_calculator_vs.PNG)

#### Screenshot of A1 Jupyter Notebook:

![tip_calculator.ipynb](img/a1_jupyter_notebook.png "A1 Jupyter Notebook")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/lcr18r/bitbucketstationlocations/ "Bitbucket Station Locations")

