# LIS4369 Extensible Enterprise Solutions 

## Lucia Ramirez

### Assignment A5 Requirements:

*three parts:*

#### This README.md file includes the following items:
1. RStudio
    - Screenshot of *learn_to_use_r* application running.
    - Screenshot of graphs from *learn_to_use_r*
    - Link to *myplotfile.pdf*

2. RStudio
    - Screenshot of *lis4369_a5* application running.
    - Screenshot of graphs from *lis4369_a5*

3. Skill Sets:
    - Skill Set 13
        - Python Sphere Volume Calculator
    - Skill Set 14
        - Python Calculator with Error Handling
    - Skill Set 15
        - Python Read Write text file


#### Assignment Screenshots:

##### 1.  *learn_to_use_r* in RStudio
| Screenshot |
| :-------- | 
| ![a5 learn_to_use_r](img/tutorial_0.png "a5 learn_to_use_r") | 

| Graph Screenshot | Graph Screenshot |
| :-------- | :-------- |
| ![a5 learn_to_use_r](img/tutorial_1.png "a5 learn_to_use_rs") | ![a5 learn_to_use_r graphs](img/tutorial_2.png "a5 learn_to_use_r graphs") |
| Link to pdf graph files of tutorial: [myplotfile.pdf](https://bitbucket.org/lcr18r/lis4369/src/master/a5/myplotfile.pdf "myplotfile.pdf")

##### 1.  *lis4369_a5* in RStudio
| Screenshot |
| :-------- | 
| ![a5 lis4369_a5](img/a5_0.png "a5 lis4369_a5") | 

| Graph Screenshot | Graph Screenshot |
| :-------- | :-------- |
| ![a5 lis4369_a5 graphs](img/a5_1.png "a5 lis4369_a5 graphs") | ![a5 lis4369_a5 graphs](img/a5_2.png "a5 lis4369_a5 graphs") |



##### 4. Skill Sets Screenshots
| Skill Set 13: Sphere Volume | Skill Set 14: Calculator | 
| :--------- | : --------- |  
| ![Skill Set 13 Screenshot](img/ss13.png "Skill Set 13") | ![Skill Set 14 Screenshot](img/ss14a.png "Skill Set 14") |


| Skill Set 15: Read-Write File | 
| : --------- |
| ![Skill Set 15 Screenshot](img/ss15.png "Skill Set 15") |


