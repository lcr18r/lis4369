# Pandas = "Python Data Analysis Library"
# Be sure to: pop install pandas-datareader
import datetime
import pandas_datareader as pdr # remote data access for pandas
import matplotlib.pyplot as plt 
from matplotlib import style

print("\nDeveloper: Lucia Ramirez")
def get_requirements():
    print("Data Analysis 1")
    print("\nProgram Requirements: \n"
          + "1. Run demo.py.\n"
          + "2. If errors, more than likely missing installations.\n"
          + "3. Test Python Package Installer: pip freeze.\n"
          + "4. Reasearch how to do the following installations:\n"
          + "\ta. pandas (only if missing).\n"
          + "\tb. pandas-datareader (only if missing).\n"
          + "\tb. matplotlib (only if missing).\n"
          + "5. Create at least three functions that are called by the program:\n"
          + "\ta. main(): calls at least two other functions.\n"
          + "\tb. get_requirements(): displays the program requirements.\n"
          + "\tc. data_analysis_1(): displays the following data.\n")
          

def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()
    
    # Read data into Pandas DataFrame, implicitly created by DataReader 
    # Note: XOM is stock market sympbol for Exxon MObil Corporation
    
    df = pdr.DataReader("XOM", "yahoo", start, end)
    
    print("\nPrint number of records: ")
    print(len(df))
    
    print("\nPrint columns: ")
    print(df.columns)
    
    print("\nPrint data frame: ")
    print(df) #Note: for efficiency, only prints 60--not *all* records
    
    print("\nPrint first five lines: ")
    # note: "date" is lower than the other columns as it is treated as an index
    print(df.head()) # head() prints top 5 rows. Here, with 7 columns
    
    print("\nPrint last five lines: ")
    print(df.tail())
        
    print("\nPrint first 2 lines: ")
    print(df.head(2))
        
    print("\nPrint last 2 lines: ")
    print(df.tail(2))
    
    #Research what these styles do!
    # style.use('fivethirtyeight')
    # compare with
    style.use('ggplot')
    
    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()
