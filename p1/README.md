# LIS4369 Extensible Enterprise Soluctions 

## Lucia Ramirez

### Assignment P1 Requirements:

*four parts:*

#### This README.md file includes the following items:
1. Data Analysis in Terminal
    - Screenshot of *Data Analysis* application running.

2. Link to p1_data_analysis.ipynb file: [data_analysis.ipynb](https://bitbucket.org/lcr18r/lis4369/src/master/p1/data_analysis.ipynb "data_analysis.ipynb").

3. Data Analysis in Jupyter Notebook

4. Skill Sets:
    - Skill Set 7
        - Python Using Lists
    - Skill Set 8
        - Python Using Tuples
    - Skill Set 9
        - Python Using Sets


#### Assignment Screenshots:

##### 1.  Data Analysis in Terminal
| Screenshot |
| :-------- | 
| ![P1 Data Analysis](img/p1_1.png "P1 Data Analysis") | 
| ![P1 Data Analysis](img/p1_2.png "P1 Data Analysis") |
| Plot Screenshot |
| ![P1 Data Analysis Plots](img/p1_3.png "P1 Data Analysis Plots") |

##### 3.  Data Analysis in Jupyter Notebook Screenshots:
| Code | Output |
| :-------- | :_----- |
|![Data Analysis Code in Jupyter Notebook](img/p1_jn_1.png "Data Analysis Code") | ![Data Analysis Output Jupyter Notebook](img/p1_jn_2.png "Data Analysis")


##### 4. Skill Sets Screenshots
| Skill Set 7: Lists | Skill Set 8: Tuples | Skill Set 9: Sets | 
| :--------- | : --------- | : --------- |
| ![Skill Set 7 Screenshot](img/ss7.png "Skill Set 7") | ![Skill Set 8 Screenshot](img/ss8.png "Skill Set 8") |![Skill Set 9 Screenshot](img/ss9.png "Skill Set 9") |





