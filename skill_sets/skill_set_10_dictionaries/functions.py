print("\nDeveloper: Lucia Ramirez")
def get_requirements():
    print("Python Dictionaries")
    print("\nProgram Requirements: \n"
          + "1. Dictionaries (Python data structure): unordered key:value parts.\n"
          + "2. Dictionary: an associative array (also known as hashes).\n"
          + "3. Any key in a dictionary is ossociated (or mapped) to a value (i.e., any Python data type).\n"
          + "4. Keys: must be of immutable type (string, number or tuple with immutable elements) and must be unique.\n"
          + "5. Values: can be any data type and can repeat\n"
          + "6. Create a program that mirrors the follwoing IPO (input/process/output) format.\n"
          + "\tCreate empty dictionary, using curly braces {}: my_dictionary = {}\n"
          + "\tUes the following keys: fname, lname, degree, major, gpa\n"
          + "Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set.\n")
    
def using_dictionaries():
    # Create empty dictionary
    my_dictionary = {}

    # User input
    print("Input:")
    my_dictionary['fname'] = input("First Name: ")
    my_dictionary['lname'] = input("Last Name: ")
    my_dictionary['degree'] = input("Degree: ")
    my_dictionary['major'] = input("Major (IT or ICT): ")
    my_dictionary['gpa'] = input("GPA: ")
    
    
    print("\nOutput:")
    print("Print my_dictionary: ")
    print(my_dictionary)  

    print("\nReturn view of dictionary's (key, value) pair, builtin function: ") 
    print(my_dictionary.items()) 

    print("\nReturn view object of all keys, built-in function:")
    print(my_dictionary.keys())

    print("\nReturn view object of all values in dictionary, built-in function:")
    print(my_dictionary.values())

    print("\nPrint only first and last names, using keys:")
    print(my_dictionary["fname"], my_dictionary["lname"])
    
    print("\nPrint only first and last names, using get() function: ")
    print(my_dictionary.get("fname"), my_dictionary.get("lname"))

    print("\nCount number of items (key:value pairs) in dictionary")
    print(len(my_dictionary))

    print("\nRemove last dictionary item (popitem): ")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary, using key: ")
    my_dictionary.pop("major")
    print(my_dictionary)

    print("\nReturn object type: ")
    print(type(my_dictionary))

    print("\nDelete all items from list: ")
    my_dictionary.clear()
    print(my_dictionary)

    del my_dictionary

