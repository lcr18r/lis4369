"""Defines three functions:
    
1. get_requirements()
2. using_lists()
"""
print("\nDeveloper: Lucia Ramirez")
def get_requirements():
    print("Python Lists")
    print("\nProgram Requirements: \n"
          + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
          + "2. Lists are mutable/changeable--that is, can insert, update, and delete.\n"
          + "3. Create list - using square brakets[list]: my_list = [\"cherries\", \"apples\", \"bananas\", \"oranges\"].\n"
          + "4. Create a prgrom that mirrors the following IPO (input/process/output) format.\n"
          + "Note: user enters number of requested list elements, dynamically renedered below (that is number of elements can change each run)\n")
    
def using_lists():
    # Variables
    start = 0
    end = 0
    my_list = []    
            
    # IPO: Input > Process > Output
    # get user data
    print("Input: ")
    num = int(input("Enter number of list elements: "))

    # range(stop)
    # stop: Number of integers (whole numbers) to generate,
    # starting from zero; e.g., range(3) == [0,1,2]
    print()

    #IPO: Input > Process > Output
    # get user data
    # Input and Process
    for i in range(num): # run loop num times (0 to num)
        my_element = input("Please enter list element " + str(i + 1) + ": ")
        my_list.append(my_element) # append each element to end of list
    
    # Output
    print("\nOutput:")
    print("Print my list: ")
    print(my_list)

    elem = input("\nPlease enter list element: ")
    pos = int(
        input("Please enter list *index* position (note must convert to int:"))
    
    print("\nInsert element into specific position in my_list")
    my_list.insert(pos, elem)  # Note: pos 1 inserts item into 2nd element
    print(my_list)

    print("\nCount number of elements in list: ")
    # also works for strings, tuples, dict objects
    print(len(my_list))
    
    print("\nSort elements in list alphabetically: ")
    my_list.sort()  #sort alphabetically
    print(my_list)

    print("\nReverse list: ")
    my_list.reverse()
    print(my_list)

    print("\nRemove last list element: ")
    my_list.pop()
    print(my_list)

    print("\nDelete second element from list by *index* (note: 1 = 2nd elelment): ")
    del my_list[1]
    print(my_list)

    print("\nElement from list by *value* (cherries): ")
    my_list.remove("cherries")
    print(my_list)

    print("\nDelete all elements from list")
    my_list.clear()
    print(my_list)
    print()

    # or
    # print("\nDelete all elements from list (using slicing): ")
    # del my_list[:]  # delete all elements using slicing
    # print(my_list)
