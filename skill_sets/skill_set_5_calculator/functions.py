def get_requirements():
    print("\nDeveloper: Lucia Ramirez")
    print("Python Selection Structures")
    print("\nProgram Requirements:\n"
        + "1. Use Python Selection Structures.\n"
        + "2. Prompt user for two numbers, and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n"
        + "4. Replicate display below.\n")

def python_selection_structures():
    # initialize variables
    
    # IPO: Input > Process > Output
    # get user data
    num1 = int(input("Enter the first number: "))
    num2 = int(input("Enter the second number: "))
    print("\nSuitable operators:\n"
    "\t+ for addition\n"
    "\t- for subtraction\n"
    "\t* for multiplication\n"
    "\t/ for division\n"
    "\t// for integer division\n"
    "\t% for modulo operator\n"
    "\t** for power\n")
    operator = input("Enter operator: ")
       
    # Process:
    # calculate total number of students
    if operator == '+':
        print('{} + {} = '.format(num1, num2))
        print(num1 + num2)
 
    elif operator == '-':
        print('{} - {} = '.format(num1, num2))
        print(num1 - num2)

    elif operator == '*':
        print('{} * {} = '.format(num1, num2))
        print(num1 * num2)

    elif operator == '/':
        print('{} / {} = '.format(num1, num2))
        print(num1 / num2)

    elif operator == '//':
        print('{} // {} = '.format(num1, num2))
        print(num1 // num2)

    elif operator == '%':
        print('{} % {} = '.format(num1, num2))
        print(num1 % num2)
    
    elif operator == '**':
        print('{} ** {} = '.format(num1, num2))
        print(num1 ** num2)

    else:
        print("Incorrect operator!")
    print("")
   
    





