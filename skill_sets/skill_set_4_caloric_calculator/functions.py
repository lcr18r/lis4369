def get_requirements():
    print("\nDeveloper: Lucia Ramirez")
    print("Calorie Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein.\n"
        + "2. Calculate percentages.\n"
        + "3. Must use float data types.\n"
        + "4. Format, right-align numbers, and round to two decimal places.\n")

def calorie_percentage():
    # initialize variables
    fat_constant = 9
    carb_constant = 4
    protein_constant = 4
        

    # IPO: Input > Process > Output
    # get user data
    print("Input:")
    fat = float(input("Enter total fat grams: "))
    carb = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))
    
    # calculate information
    fat_total = fat * fat_constant
    carb_total = carb * carb_constant
    protein_total = protein * protein_constant

    fat_percentage = fat_total / (fat_total + carb_total + protein_total)
    carb_percentage = carb_total / (fat_total + carb_total + protein_total)
    protein_percentage = protein_total / (fat_total + carb_total + protein_total)
   


    # display results
    print("\nOutput:")
    print("{0:8} {1:>10} {2:>13}".format("Type", "Calories", "Percentage"))
    print("{0:8} {1:>10,.2f} {2:13.2%}".format("Fat", fat_total, fat_percentage))
    print("{0:8} {1:>10,.2f} {2:13.2%}".format("Carbs", carb_total, carb_percentage))
    print("{0:8} {1:>10,.2f} {2:13.2%}".format("Protein", protein_total, protein_percentage))
    print("")
    





