import functions as f

def main():
    f.get_requirements()
    #f.user_input()
    f.calculate_sqft_to_acre()
    
    
# global variable, __name__, in mudule is entry point ot program, that is, "__main__". Otherwise it's the name you import he module by.
# code under if block will only run if module is entry point to your program
# It allows code in module to be importable by other modules, without executing code block beneath on import.
# In short, use 'if__name__ == "main"' block to prevent (certain) code from being run when the module is imported.
# Put simply, __name__ is a variable defined for each script that defines whether the script is being run as the main module or it is being run as an imported module.

# Executes main() function only if this file is executed as the main program.

if __name__ == "__main__":
    main()