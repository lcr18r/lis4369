def get_requirements():
    print("Developer: Lucia Ramirez")
    print("IT/ICT Student Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find number of IT/ICT students in class.\n"
        + "2. Calculate IT/ICT Student Percentage.\n"
        + "3. Must use float data type (to facilitate right-alignment).\n"
        + "4. Format, right-align number, and round to two decimal places.\n")

def calculate_it_ict_student_percentage():
    # initialize variables
    it = 0
    ict = 0
    total_students = 0
    percentage_it = 0.0
    percentage_ict = 0.0

    # IPO: Input > Process > Output
    # get user data
    print("Input: ")
    it = int(input("Enter number of IT students: "))
    ict = int(input("Enter number of ICT students: "))

    # Process:
    # calculate total number of students
    total_students = it + ict

    # calculate percentage of IT students
    # Python 3: integer division implicitly casts to float
    percentage_it = it / total_students

    # calculate percentage of IT students
    percentage_ict = ict / total_students

    # print output
    print("Output: ")
    print("{0:17} {1:>5.2f}".format("Total Students: ", total_students))
    print("{0:17} {1:>5.2%}".format("IT Students: ", percentage_it))
    print("{0:17} {1:>5.2%}".format("ICT Students: ", percentage_ict))






