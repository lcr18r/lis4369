# LIS4369 Extensible Enterprise Solutions (Python/R Data Analytics/Visualization)

## Lucia Ramirez

### Class lis4369 Table of Contents:

*Course Work Links:*

### Assignments
1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Instal Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1_tip_calculator* Jupyter Notebook
    - Provide screenshots of installations
    - Create BitBucket repo
    - Complete BitBucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Payroll app using "Separation of Concerns" design principles
    - Provide screenshots of completed *Payroll Calculator*
    - Provide screenshots of completed Python Skill Sets 1, 2, and 3
    - Provide screenshots of completed Jupyter Notebook
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Painting Estimator in Terminal
    - Skill Sets:
        - Skill Set 4
            - Python Calorie Calculator
        - Skill Set 5
            - Python Calculator
        - Skill Set 6
            - Loops in Python
    - Painting Estimator in Jupyter Notebook

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Link to data_analysis_2.ipynb file.
    
    - Screenshot of *Data Analysis 2* application running.
    
    - Data Analysis 2 in Jupyter Notebook

    - Skill Sets:
        - Skill Set 10
            - Python Using Dictionaries
        - Skill Set 11
            - Pseudo Random Number Generator
        - Skill Set 12
            - Temperature Converter

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - RStudio
        - Screenshot of *learn_to_use_r* application running.
        - Screenshot of graphs from *learn_to_use_r*
        - Link to *myplotfile.pdf*

    - RStudio
        - Screenshot of *lis4369_a5* application running.
        - Screenshot of graphs from *lis4369_a5*

    - Skill Sets:
        - Skill Set 13
            - Python Sphere Volume Calculator
        - Skill Set 14
            - Python Calculator with Error Handling
        - Skill Set 15
            - Python Read Write text file

### Projects
1. [P1 README.md](p1/README.md "My p1 README.md file")
    - Data Analysis in Terminal
        - Screenshot of *Data Analysis* application running.
    
    - Link to *p1_data_analysis.ipynb* file
    
    - Data Analysis in Jupyter Notebook
    
    - Skill Sets:
        - Skill Set 7
            - Python Using Lists
        - Skill Set 8
            - Python Using Tuples
        - Skill Set 9
            - Python Using Sets   

2. [P2 README.md](p2/README.md "My p2 README.md file")
    - RStudio: Motor Trend Car Road Tests data analysis
        - Screenshots of *Lucia_Ramirez_p2_requirements* text file output 
        - Link to *Lucia_Ramirez_p2_requirements* text file
        - Screenshots of graphs from p2 output, including developer name.

    - RStudio: Annual Developer Survey data analysis
        - Screenshot of *Annual Developer Survey* running.
        - Screenshots of graphs from *Annual Developer Survey myplotfile pdf* 
        - Link to myplotfile.pdf
