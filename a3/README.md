# LIS4369 Extensible Enterprise Soluctions

## Lucia Ramirez

### Assignment 3 Requirements:

*three parts:*

1. Painting Estimator in Terminal

2. Skill Sets:
    - Skill Set 4
        - Python Calorie Calculator
    - Skill Set 5
        - Python Calculator
    - Skill Set 6
        - Loops in Python

3. Painting Estimator in Jupyter Notebook

#### This README.md file includes the following items:

* Screenshot of *Painting Estimator* application running.
* Link to A3 Painting Estimator.ipynb file: [painting_estimator.ipynb](https://bitbucket.org/lcr18r/lis4369/src/master/a3/painting_estimator.ipynb "Painting Estimator.ipynb").
* Provide screenshots for completed python skill sets: 4, 5, and 6.
* Screensot of *Painting Estimator* application in Jupyter Notebook.

#### Assignment Screenshots:

##### Painting Estimator in Terminal
| First run through loop | Continuation of loop |
| :-------- | :------ |
|![A3 Painting Estimator](img/a3_1.png =100x20 "A3 A3 Painting Estimator before loop") | ![A3 Painting Estimator](img/a3_2.png "A3 Painting Estimator after loop")

##### 2. Skill Sets Screenshots
| Skill Set 4: Calorie Calculator | Skill Set 5: Valid Operator | Skill Set 5: Invalid Operator | 
| :--------- | : --------- | : --------- |
| ![Skill Set 4 Screenshot](img/ss4.PNG "Skill Set 4") | ![Skill Set 5 Screenshot](img/ss5_valid.PNG "Skill Set 5") |![Skill Set 5 Screenshot](img/ss5_invalid.PNG "Skill Set 5") |

| Skill Set 6: Loops 1-4 | Skill Set 6: Loops 5 - 9 |
| :--------- | : --------- |
![Skill Set 6 Screenshot](img/ss6_1.png "Skill Set 6") | ![Skill Set 6 Screenshot](img/ss6_2.png "Skill Set 6")|

### Painting Estimator in Jupyter Notebook Screenshots:
| Code | Output |
| :-------- | :_----- |
|![Painting Estimator Code in Jupyter Notebook](img/a3_jn_1.png "Painting Estimator Code") | ![Painting Estimator Output Jupyter Notebook](img/a3_jn_2.png "Painting Estimator Output")
