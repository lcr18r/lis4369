import functions as f

def main():
    f.get_requirements()
    #user_input = f.get_user_info()
    check = "y"
    while check.lower() == "y":
        f.estimate_painting_cost()
        print()
        check = input("Estimate another paint job? (y/n)")
        print()
    
    print("Thank you for using our Painting Estimator!")
    
if __name__ == "__main__":
    main()