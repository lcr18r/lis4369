"""Defines three functions:
    
1. get_requirements()
2. painting_estimator()
3. print_pay()
"""
print("\nDeveloper: Lucia Ramirez")
def get_requirements():
    print("Painting Estimator")
    print("\nProgram Requirements: \n"
          + "1. Calculate home interior paint cost (w/o primer) \n"
          + "2. Must use float data types.\n"
          + "3. Must use SQFT_PER_GALLON constant (350).\n"
          + "4. Must use iteration structure (aka \"loop\"). \n"
          + "5. Format, right-align numbers, and round to two decimal places. \n"
          + "6. Creat at least five functions that are called by the program: \n"
          + "\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost(). \n"
          + "\tb. get_requirements(): displays the program requirements.\n"
          + "\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
          + "\td. print_painting_estimate(): displays painting costs. \n"
          + "\te. print_painting_percentage(): displays painting costs percentages. \n")
    
def estimate_painting_cost():
    # Variables
    SQFT_PER_GALLON = 350  
    total_sqft = 0.0       
        
    # IPO: Input > Process > Output
    # get user data
    print("Input: ")
    total_sqft = float(input('Enter interior sq ft: '))
    price_per_gallon = float(input('Enter price per gallon paint: '))
    pay_rate = float(input('Enter hourly painting rate per sq ft: '))
    
    # Process:
    # Calucations
    paint_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = paint_gallons * price_per_gallon
    labor_cost = total_sqft * pay_rate
    total_cost = paint_cost + labor_cost
    paint_percentage = paint_cost / total_cost
    labor_percentage = labor_cost / total_cost

    # display painting estimate    
    print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, pay_rate)

    # display painting percentage
    print_painting_percentage(paint_cost, paint_percentage, labor_cost, labor_percentage, total_cost)
        
def print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, pay_rate):
    print("\nOutput: ")
    print("{0:20} {1:>9}".format("Item", "Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft", total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per Gallon", SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of Gallons", paint_gallons))
    print("{0:20} ${1:8,.2f}".format("Paint per Gallon", price_per_gallon))
    print("{0:20} ${1:8,.2f}".format("Labor per Sq Ft", pay_rate))
    print()

def print_painting_percentage(paint_cost, paint_percentage, labor_cost, labor_percentage, total_cost):
    print("{0:8} {1:>9} {2:>13}".format("Cost", "Amount", "Percentage"))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Paint:", paint_cost, paint_percentage))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Labor:", labor_cost, labor_percentage))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Total:", total_cost, paint_percentage + labor_percentage))
    
    
    
    