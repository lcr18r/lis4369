# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 16:50:11 2020

@author: LucyK
"""

#phone_number = input("Enter phone number: ").strip() 
#if len(phone_number) == 10: 
#   phone_number = "(" + phone_number[:3] + ")" + phone_number[3:6] + "-" + phone_number[6:]
#  print("Phone number: ", phone_number) 
# else: 
#      print("Phone number: ", "Phone number must be 10 digits")


# s1 = "abc def ghi";
# s2 = s1[1:5]
# s3 = s2.replace('b', 'z')
# print(s3)


# s1 = "118-45-9271"
# s2 = ""
# for i in s1:
#     if i != '-':
#         s2 += i
# s1.replace("-", ".")
# print(s2)

# email = "marytechknowsolve.com" 
# result = email.find("@")
# print(result)

# email = "joel.murach@com" 
# result = email.find("@") - email.find(".") 
# print(result)

# book_name = "a tale for the knight" 
# book = book_name.title() 
# print(book)


# print("B = ", ord("B")) # B =  66
# print("B = ", ascii("B"))  # B =  'B'
# print(ord(66)) # TypeError: ord() expected string of length 1, but int found

# counting = "54321"
# print("Countdown...") 
# for char in counting: 
#     print(char + "...")
# print("Blastoff!")



